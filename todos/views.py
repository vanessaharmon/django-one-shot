from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm

# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList,id=id)
    context = {
        "todo_item": todo,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_edit(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)

        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

    else:
        form = TodoListForm(instance=todolist)

    context = {
        "todolist": todolist,
        "form": form,
    }

    return render(request, "todos/edit.html", context)
